 <h1>Pipeline CI/CD do GitLab</h1>
<p>Este repositório contém um pipeline GitLab CI/CD para automatizar a implantação de infraestrutura usando Terraform e a construção de imagens Docker. O pipeline consiste em várias etapas, desde a inicialização do Terraform até o envio das imagens Docker para o Docker Hub.</p>

<h2>Comandos</h2>

<h3>Terraform</h3>
<p><strong>Inicializar Terraform:</strong></p>
<code>terraform init</code>
 <p>Este comando inicializa o Terraform e prepara o diretório de trabalho para uso.</p>

<p><strong>Planejar Alterações:</strong></p>
<code>terraform plan</code>
 <p>Este comando cria um plano de execução que mostra as alterações que o Terraform fará quando <code>terraform apply</code> for executado.</p>

<p><strong>Aplicar Alterações:</strong></p>
<code>terraform apply -auto-approve</code>
  <p>Este comando aplica as alterações definidas nos arquivos Terraform.</p>

<h3>Docker</h3>
<p><strong>Construir uma Imagem Docker:</strong></p>
<code>docker build -t nome_imagem .</code>
   <p>Este comando constrói uma imagem Docker com base no Dockerfile no diretório atual.</p>

<p><strong>Fazer o Push de uma Imagem Docker:</strong></p>
<code>docker push nome_imagem</code>
 <p>Este comando envia a imagem Docker para um registro de contêiner, como o Docker Hub.</p>

  <h2>Dificuldades e Explicações Adicionais</h2>
<ul>
        <li><strong>Autenticação da AWS:</strong> Certifique-se de fornecer credenciais válidas para autenticação com a AWS.</li>
        <li><strong>Autenticação no Docker Hub:</strong> Configure as credenciais de acesso ao Docker Hub como variáveis de ambiente no GitLab.</li>
        <li><strong>Configuração dos Dockerfiles:</strong> Verifique se os Dockerfiles estão configurados corretamente e se todos os arquivos necessários estão presentes nos diretórios correspondentes.</li>
</ul>
<h2>Estrutura do Pipeline</h2>
 <p>O pipeline é composto por várias etapas, cada uma responsável por uma parte específica do processo:</p>
<ol>
        <li>
            <strong>Terraform:</strong>
            <ul>
                <li><strong>Inicialização:</strong> Inicializa o Terraform e prepara o ambiente para a execução dos comandos.</li>
                <li><strong>Planejamento de Alterações:</strong> Gera um plano de execução mostrando as alterações que serão aplicadas na infraestrutura.</li>
                <li><strong>Aplicação de Alterações:</strong> Aplica as alterações na infraestrutura, conforme definido nos arquivos Terraform.</li>
            </ul>
        </li>
        <li>
            <strong>Docker:</strong>
            <ul>
                <li><strong>Construção das Imagens Docker:</strong> Constrói as imagens Docker com base nos Dockerfiles presentes nos diretórios <code>./app</code> e <code>./app2</code>.</li>
     <li><strong>Envio das Imagens para o Docker Hub:</strong> Envia as imagens Docker para o Docker Hub, permitindo o compartilhamento e distribuição das mesmas.</li>
            </ul>
        </li>
</ol>

 <h2>Aplicação Web</h2>
 <p>O código HTML e PHP fornecido neste repositório é um exemplo básico de uma aplicação web que insere dados em um banco de dados MySQL. Ele também inclui uma conexão de banco de dados PHP usando as credenciais fornecidas.</p>

 <h2>Conclusão</h2>
 <p>Este pipeline fornece uma maneira eficiente e automatizada de implantar infraestrutura na AWS e construir imagens Docker, permitindo uma integração e entrega contínuas em seus projetos. Certifique-se de personalizar e ajustar o pipeline conforme necessário para atender às necessidades específicas do seu projeto.</p>
